<?php

/**
 * Drupal starter theme provided by gazwal.com - Freelance Drupal Development Services
 * It is mainly based on Basic 2.2/2.3, Zen and some personal features
 * / 

/**
 * @file block.tpl.php
 *
 * Theme implementation to display a block.
 *
 * Available variables:
 * - $block->subject: Block title.
 * - $block->content: Block content.
 * - $block->module: Module that generated the block.
 * - $block->delta: This is a numeric id connected to each module.
 * - $block->region: The block region embedding the current block.
 *
 * Helper variables:
 * - $block_zebra: Outputs 'odd' and 'even' dependent on each block region.
 * - $zebra: Same output as $block_zebra but independent of any block region.
 * - $block_id: Counter dependent on each block region.
 * - $id: Same output as $block_id but independent of any block region.
 * - $is_front: Flags true when presented in the front page.
 * - $logged_in: Flags true when the current user is a logged-in member.
 * - $is_admin: Flags true when the current user is an administrator.
 *
 * @see template_preprocess()
 * @see template_preprocess_block()
 */
?>
<div id="block-<?php echo $block->module .'-'. $block->delta ?>" class="block block-<?php echo $block->module .' '. $block_zebra .' '. $block->region ?> clearfix">
	<div class="block-inner">

		<?php if ($block->subject): ?>
		  <h3 class="block-title"><?php echo t($block->subject); ?></h3>
		<?php endif; ?>
		
		<div class="block-content">
		  <?php echo $block->content; ?>
		</div>
		
	  <?php echo $edit_links; ?>

	</div> <!-- /block-inner -->
</div> <!-- /block -->