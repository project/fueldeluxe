<?php

/**
 * Drupal starter theme provided by gazwal.com - Freelance Drupal Development Services
 * It is mainly based on Basic 2.2/2.3, Zen and some personal features
 * / 

/**
 * @file box.tpl.php
 *
 * Theme implementation to display a box.
 *
 * Available variables:
 * - $title: Box title.
 * - $content: Box content.
 *
 * @see template_preprocess()
 */
?>
<div class="box">
    
    <?php if ($title): ?>
      <h3><?php echo $title ?></h3>
    <?php endif; ?>
    
    <div class="content">
      <?php echo $content ?>
    </div>

</div>
