<?php
/**
 * @file views-view-list.tpl.php
 * Default simple view template to display a list of rows.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $options['type'] will either be ul or ol.
 * @ingroup views_templates
 */
?>

<?php
  // print '<pre>'.print_r(array_keys(get_defined_vars()), 1).'</pre>'; // debug OK
  // dsm(array_keys(get_defined_vars())); // debug OK, idem ci-dessus mais en version Krumo
  // dpm( get_defined_vars() ); // debug OK, idem ci-dessus + explo de l'arbo

  // dpm($rows); // debug OK, pour cibler les rows
?>

<div class="item-list">
  <?php if (!empty($title)) : ?>
    <h3><?php print $title; ?></h3>
  <?php endif; ?>
  <<?php print $options['type']; ?>>
    <?php foreach ($rows as $id => $row): ?>
      <li class="<?php print $classes[$id]; ?>"><?php print $row; ?></li>
    <?php endforeach; ?>
  </<?php print $options['type']; ?>>
</div>