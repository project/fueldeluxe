<?php
/**
 * @file views-view-unformatted.tpl.php
 * Default simple view template to display a list of rows.
 *
 * @ingroup views_templates
 */
?>

<?php
  // print '<pre>'.print_r(array_keys(get_defined_vars()), 1).'</pre>'; // debug OK
  // dsm(array_keys(get_defined_vars())); // debug OK, idem ci-dessus mais en version Krumo
  // dpm( get_defined_vars() ); // debug OK, idem ci-dessus + explo de l'arbo

  // dpm($rows); // debug OK, pour cibler les rows
?>


<?php if (!empty($title)): ?>
  <h3><?php print $title; ?></h3>
<?php endif; ?>
<?php foreach ($rows as $id => $row): ?>
  <div class="<?php print $classes[$id]; ?>">
    <?php print $row; ?>
  </div>
<?php endforeach; ?>
